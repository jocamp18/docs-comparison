#!/bin/env python3.6

"""@file
Contiene el algoritmo serial de análisis de distancia entre documentos, así como
algunas funciones que se encargan de realizar el proceso ETL dado un
conjunto de datos de entrada.
"""

import sys
sys.path.append("/h/grupo13/python_packages/lib/python3.6/site-packages")
import re
import sys
from collections import Counter
from operator import itemgetter
import numpy as np
from langdetect import detect
from nltk.corpus import stopwords
from nltk.stem import SnowballStemmer
from nltk.tokenize import RegexpTokenizer
from math import sqrt
import pickle
import os

spanish_stemmer = SnowballStemmer('spanish')
english_stemmer = SnowballStemmer('porter')
spanish_stop_words = stopwords.words('spanish')
english_stop_words = stopwords.words('english')
tokenizer = RegexpTokenizer(r'\w+')

magnitudes = []
bag_words_list = []
similarity_matrix = None
documents = []


def tokenize(content):
    """Dado un conjunto de palabras contenidas
    en un mismo arreglo de caracteres, la función
    elimina aquellas que se consideren irrelevantes (stop words),
    normaliza el contenido de la entrada (normalization) y obtiene
    las raices comunes (stemming).

    @param content String al cual se le aplicarán los procesos
    de tokenization, normalization y stemming.

    @return Una lista con las palabras relevantes que hacen parte
    de la variable de entrada.
    """
    content = re.sub(r'\d+', '', content)
    language = detect(content)
    stemmed_words = []
    words = tokenizer.tokenize(content)
    if language == 'es':
        for w in words:
            if w not in spanish_stop_words:
                stemmed_words.append(spanish_stemmer.stem(w))
    else:
        for w in words:
            if w not in english_stop_words:
                try:
                    stemmed_words.append(english_stemmer.stem(w))
                except:
                    continue

    return stemmed_words


def word_counter(document):
    """Cuenta el número de ocurrencias de cada palabra en un
    documento y genera un diccionario con los elementos:
    llave: palabra, valor: número de ocurrencias.

    @param document Nombre de uno de los documentos.

    @return None
    """
    with open(document, "r", encoding="utf-8") as f:
        doc_content = f.read()
        stemmed_words = tokenize(doc_content)

    words_dic = dict(Counter(stemmed_words))

    bag_words_list.append(words_dic)


def create_matrix():
    """Crea la matriz de similaridad entre documentos.

    @return None
    """
    global similarity_matrix
    n = len(bag_words_list)
    similarity_matrix = np.zeros((n, n), dtype='float')
    for i in range(0, n):
        similarity_matrix[i][i] = -1
        for j in range(i + 1, n):
            similarity_matrix[i][j] = jaccard_coefficient(i, j)

def jaccard_coefficient(i, j):
    """Obtiene el coeficiente Jaccard entre dos documentos.

    @param i Número relativo a un documento.
    @param j Número relativo a un documento cuya similaridad se desea
    comparar con otro representado por el valor i.

    @return Número de punto flotante con el resultado obtenido a partir
    de los calculos realizados por la función.
    """
    global magnitudes
    dict1 = bag_words_list[i]
    dict2 = bag_words_list[j]

    dot_product = sum(dict1[key] * dict2.get(key, 0) for key in dict1)
    result = dot_product / (pow(magnitudes[i], 2) + pow(magnitudes[j], 2) - dot_product)
    return result


def cosine_similarity(i, j):
    """Obtiene el coeficiente Coseno entre dos documentos.

    @param i Número relativo a un documento.
    @param j Número relativo a un documento cuya similaridad se desea
    comparar con otro representado por el valor i.

    @return Número de punto flotante con el resultado obtenido a partir
    de los calculos realizados por la función.
    """
    global magnitudes
    dict1 = bag_words_list[i]
    dict2 = bag_words_list[j]

    dot_product = sum(dict1[key] * dict2.get(key, 0) for key in dict1)

    result = dot_product / (pow(magnitudes[i], 2) + pow(magnitudes[j], 2))
    return result


def get_magnitudes():
    """Aplica una norma sobre un conjunto de diccionarios que conforman
    la bolsa de palabras global de todos los documentos.

    @return None
    """
    global magnitudes
    n = len(bag_words_list)
    magnitudes = [0] * n
    for i in range(0, n):
        for key, value in bag_words_list[i].items():
            magnitudes[i] += pow(value, 2)
        magnitudes[i] = sqrt(magnitudes[i])


def get_top(top):
    """Recorre la matriz de similiridad en busca de una cantidad especifica
    de documentos que se relacionen con otros de su mismo conjunto de datos,
    para finalmente imprimir el número de relaciones indicadas por el
    argumento de la función.

    @param top Cantidad de relaciones que se desean obtener para cada
    documento.

    @return None
    """
    n = len(similarity_matrix[0])
    if n < top:
        print("Error, not enough documents to get top {}".format(top))
        exit(1)

    for k in range(0, n):
        distance_list = []
        for i in range(0, k):
            distance_list.append(similarity_matrix[i][k])

        for j in range(k, n):
            distance_list.append(similarity_matrix[k][j])

        result = zip(documents, distance_list)
        sorted_result = sorted(result, key=itemgetter(1), reverse=True)
        document, value = zip(*sorted_result)
        document = list(document)
        docs = [os.path.split(doc)[1] for doc in document[:top]]
        print("Document: {} -> {}".format(os.path.split(documents[k])[1], docs))


def main(argv):
    """Comienza la ejecución del programa dados los argumentos contenidos
    en argv.

    @param argv Argumentos pasados por el usuario al programa.

    @return None
    """
    global similarity_matrix, bag_words_list, documents
    if len(argv) < 2:
        print("Error, not documents passed")
        exit(1)

    top = int(argv[1])

    with open(argv[2], "r") as file:
        for doc_name in file:
            documents.append(doc_name.rstrip())

    if not os.path.exists("bags_word"):
        for document in documents:
            word_counter(document)

        print("Documents Ready")

        get_magnitudes()
        print("Magnitudes Ready")

        create_matrix()
        print("Matrix Ready")

        with open("bags_word", "wb") as f:
            pickle.dump(bag_words_list, f)

        with open("similarity_matrix", "wb") as f:
            pickle.dump(similarity_matrix, f)
    else:
        with open("bags_word", "rb") as f:
            bag_words_list = pickle.load(f)

        with open("similarity_matrix", "rb") as f:
            similarity_matrix = pickle.load(f)

    get_top(top)


if __name__ == '__main__':
    main(sys.argv)
