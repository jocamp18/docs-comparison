# docs-comparison

El presente proyecto básicamente se encarga de realizar un análisis de similitud entre diferentes documentos utilizando el algoritmo de Jaccard y da como resultado los "n" documentos que más parentezco tengan entre si (n es un parametro dado por el usuario).

## Prerrequisitos

* Python3 o superior
* pip3
* NLTK
* Langdetect
* OpenMPI
* mpi4py
* Numpy

## Ejecución

### Serial

```
$ python3 docs_comparison.py n documentos.txt
```

En el comando anterior se puede ver que el programa serial recibe dos parámetros:

**n:** Este es el número de documentos con mayor similitud que va a retornar por cada documento. Es decir, dado un conjunto de documentos con su respectiva similitud se retorna los "n" documentos que más parecido tengan, en otras palabras, es el top.

**documentos.txt:** Es un archivo que tiene las rutas de todos los documentos que van a ser comparados.

### Paralelo

```
$ mpirun -np procesos python3 parallel_docs_comparison.py n documentos.txt
```

Al igual que en el caso anterior, el programa en paralelo también recibe dos parámetros, y de hecho, son los mismos:

**n:** Este es el número de documentos con mayor similitud que va a retornar por cada documento. Es decir, dado un conjunto de documentos con su respectiva similitud se retorna los "n" documentos que más parecido tengan, en otras palabras, es el top.

**documentos.txt:** Es un archivo que tiene las rutas de todos los documentos que van a ser comparados.

Además, bajo el estandar MPI se recibe el número de procesos que van a trabajar en la ejecución del programa, el cual también es decisión del usuario y depende de los recursos que tenga la máquina donde se ejecutará el programa. Como recomendación para elegir el número de procesos, se debería escoger un número divisor de la cantidad de documentos con los cuales se va a trabajar.