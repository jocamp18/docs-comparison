var searchData=
[
  ['compute_5fsimilarity',['compute_similarity',['../parallel__docs__comparison_8py.html#a7b0dac714bd9efb8c922d37ef0799586',1,'parallel_docs_comparison']]],
  ['cosine_5fsimilarity',['cosine_similarity',['../docs__comparison_8py.html#a0337ff168b6bf6b97eb5f6a1d6003d10',1,'docs_comparison.cosine_similarity()'],['../parallel__docs__comparison_8py.html#ac6626633e4bee6b269f5668de44379fc',1,'parallel_docs_comparison.cosine_similarity()']]],
  ['create_5flist',['create_list',['../parallel__docs__comparison_8py.html#aea42fca8e9b69131141bdbfcf0b604a9',1,'parallel_docs_comparison']]],
  ['create_5fmatrix',['create_matrix',['../docs__comparison_8py.html#a6de0b4620547e2fe9b4dcecfa5f5fb59',1,'docs_comparison.create_matrix()'],['../parallel__docs__comparison_8py.html#ad4cae71c6b411875250cca8a3f6ef734',1,'parallel_docs_comparison.create_matrix()']]]
];
