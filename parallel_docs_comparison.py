#!/bin/env python
# -*- coding: utf-8 -*-

"""@file
Contiene el algoritmo paralelo de análisis de distancia entre documentos,
así como algunas funciones que se encargan de realizar el proceso ETL dado un
conjunto de datos de entrada.
"""

import re
import sys
from collections import Counter
from operator import itemgetter
import numpy as np
from langdetect import detect
from nltk.corpus import stopwords
from nltk.stem import SnowballStemmer
from nltk.tokenize import RegexpTokenizer
from math import sqrt
import pickle
import os
from time import time
from mpi4py import MPI
import itertools

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

spanish_stemmer = SnowballStemmer('spanish')
english_stemmer = SnowballStemmer('porter')
spanish_stop_words = stopwords.words('spanish')
english_stop_words = stopwords.words('english')
tokenizer = RegexpTokenizer(r'\w+')


def tokenize(content):
    """Dado un conjunto de palabras contenidas
    en un mismo arreglo de caracteres, la función
    elimina aquellas que se consideren irrelevantes (stop words),
    normaliza el contenido de la entrada (normalization) y obtiene
    las raices comunes (stemming).

    @param content String al cual se le aplicarán los procesos
    de tokenization, normalization y stemming.

    @return Una lista con las palabras relevantes que hacen parte
    de la variable de entrada.
    """
    content = re.sub(r'\d+', '', content)
    language = detect(content)
    stemmed_words = []
    words = tokenizer.tokenize(content)
    if language == 'es':
        for w in words:
            if w not in spanish_stop_words:
                stemmed_words.append(spanish_stemmer.stem(w))
    else:
        for w in words:
            if w not in english_stop_words:
                try:
                    stemmed_words.append(english_stemmer.stem(w))
                except:
                    continue

    return stemmed_words


def word_counter(document, bags_words):
    """Cuenta el número de ocurrencias de cada palabra en un
    documento y genera un diccionario con los elementos:
    llave: palabra, valor: número de ocurrencias.

    @param document Nombre de uno de los documentos.
    @param bags_words Bolsa de palabras global.

    @return None
    """
    with open(document, 'r', encoding='utf-8') as f:
        doc_content = f.read()
        stemmed_words = tokenize(doc_content)

    words_dic = dict(Counter(stemmed_words))

    bags_words.append(words_dic)


def create_matrix(pos_list, n):
    """Crea la matriz de similaridad entre documentos.

    @param pos_list Lista de posiciones a computar.
    @param n Cantidad de documentos a procesar.

    @return None
    """
    similarity_matrix = np.zeros((n, n), dtype="float")
    for i in range(0, len(pos_list)):
        x = pos_list[i][0][0]
        y = pos_list[i][0][1]
        similarity_matrix[x][y] = pos_list[i][1]
    return similarity_matrix


def jaccard_coefficient(pos, bags_words, magnitudes):
    """Obtiene el coeficiente Jaccard entre dos documentos.

    @param pos Lista de posisiones a procesar.
    @param bags_words Bolsa de palabras global.
    @param magnitudes Lista de magnitudes utilizadas para el cómputo del
    coeficiente.

    @return Número de punto flotante con el resultado obtenido a partir
    de los calculos realizados por la función.
    """
    i = pos[0]
    j = pos[1]
    dict1 = bags_words[i]
    dict2 = bags_words[j]

    dot_product = sum(dict1[key] * dict2.get(key, 0) for key in dict1)

    result = dot_product / (pow(magnitudes[i], 2) + pow(magnitudes[j], 2) - dot_product)
    return result


def cosine_similarity(pos, bags_words, magnitudes):
    """Obtiene el coeficiente Coseno entre dos documentos.

    @param pos Lista de posisiones a procesar.
    @param bags_words Bolsa de palabras global.
    @param magnitudes Lista de magnitudes utilizadas para el cómputo del
    coeficiente.

    @return Número de punto flotante con el resultado obtenido a partir
    de los calculos realizados por la función.
    """
    i = pos[0]
    j = pos[1]
    dict1 = bags_words[i]
    dict2 = bags_words[j]

    dot_product = sum(dict1[key] * dict2.get(key, 0) for key in dict1)

    result = dot_product / (pow(magnitudes[i], 2) + pow(magnitudes[j], 2))
    return result


def get_magnitudes(magnitudes, bags_words):
    """Aplica una norma sobre un conjunto de diccionarios que conforman
    la bolsa de palabras global de todos los documentos.

    @param magnitudes Magnitudes.
    @param bags_words Bolsa de palabras.

    @return None
    """
    for i in range(0, len(magnitudes)):
        for key, value in bags_words[i].items():
            magnitudes[i] += pow(value, 2)
        magnitudes[i] = sqrt(magnitudes[i])


def get_top(top, similarity_matrix, document_names):
    """Recorre la matriz de similiridad en busca de una cantidad especifica
    de documentos que se relacionen con otros de su mismo conjunto de datos,
    para finalmente imprimir el número de relaciones indicadas por el
    argumento de la función.

    @param top Cantidad de relaciones que se desean obtener para cada
    documento.
    @param similarity_matrix Matriz de similiradad, de la cual se extraerán
    los documentos más similares.
    @param document_names Lista con el nombre de los documentos.

    @return None
    """
    n = len(similarity_matrix[0])
    if n < top:
        print("Error, not enough documents to get top {}".format(top))
        exit(1)

    for k in range(0, n):
        distance_list = []
        for i in range(0, k):
            distance_list.append(similarity_matrix[i][k])

        for j in range(k, n):
            distance_list.append(similarity_matrix[k][j])

        result = zip(document_names, distance_list)
        sorted_result = sorted(result, key=itemgetter(1), reverse=True)
        document, value = zip(*sorted_result)
        documents = list(document)
        docs = [os.path.split(doc)[1] for doc in document[:top]]
        print("Document: {} -> {}".format(os.path.split(documents[k])[1], docs))


def create_list(n):
    """Crea una lista referente a las posiciones que cada proceso
    procesará.

    @param n Tamaño de la bolsa de palabras.

    @return Lista con las posiciones que cada proceso computará. Cada
    tupla, con valores (i, j), es seguida por su valor inicial: 0.
    """
    position_list = []
    for i in range(0, n):
        for j in range(i + 1, n):
            position_list.append([(i, j), 0])
    return position_list


def compute_similarity(pos_list, bags_words, magnitudes):
    """Computa la similaridad entre los documentos definidos en
    la lista de posiciones, maginitudes y bolsa de palabras.

    @param pos_list Lista de posiciones.
    @param bags_words Bolsa de palabras.
    @param magnitudes Lista de magnitudes.
    """
    for pos in pos_list:
        pos[1] = jaccard_coefficient(pos[0], bags_words, magnitudes)


def main(argv):
    """Comienza la ejecución del programa dados los argumentos contenidos
    en argv.

    @param argv Argumentos pasados por el usuario al programa.

    @return None
    """
    if rank == 0:
        if len(argv) < 2:
            print("Error, not documents folder passed")
            exit(1)

        top = int(argv[1])
        folder = argv[2]
        documents = []
        bags_words = []

        for root, dirs, files in os.walk(folder):
            for filename in files:
                documents.append(os.path.join(root, filename))

        split_documents = np.array_split(documents, size, axis=0)

    else:
        bags_words = []
        split_documents = None

    split_documents = comm.scatter(split_documents, root=0)

    for document in split_documents:
        word_counter(document, bags_words)

    print("Documents Ready")

    magnitudes = [0] * len(bags_words)
    get_magnitudes(magnitudes, bags_words)

    bags_words = comm.allgather(bags_words)
    bags_words = list(itertools.chain.from_iterable(bags_words))

    magnitudes = comm.allgather(magnitudes)
    magnitudes = list(itertools.chain.from_iterable(magnitudes))

    if rank == 0:
        n = len(bags_words)
        pos_list = create_list(n)
        pos_list = np.array_split(pos_list, size, axis=0)
    else:
        pos_list = None

    pos_list = comm.scatter(pos_list, root=0)
    compute_similarity(pos_list, bags_words, magnitudes)

    pos_list = comm.gather(pos_list, root=0)

    if rank == 0:
        pos_list = list(itertools.chain.from_iterable(pos_list))
        sim_matrix = create_matrix(pos_list, len(bags_words))
        print("Matrix Ready")
        get_top(top, sim_matrix, documents)


if __name__ == '__main__':
    main(sys.argv)
